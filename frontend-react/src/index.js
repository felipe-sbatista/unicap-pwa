import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import '../node_modules/bootstrap/dist/css/bootstrap.css';
import '../node_modules/bootstrap/dist/js/bootstrap';
import {BrowserRouter as Router, Route} from 'react-router-dom'
import {ListarProfessor} from "./professor/ListarProfessor";
import { Topbar } from './components/Topbar';



ReactDOM.render(
    <Router>
        <Topbar/>
        <Route path="/" exact={true} component={App}/>
        <Route path="/listar-professores" component={ListarProfessor}/>
    </Router>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();


import React, {Component} from 'react';
import './App.css';
import {Topbar} from "./components/Topbar";
import {Link} from "react-router-dom";

type Props = {}
type State = {}

const cardStyle ={
  width: '18rem'
}
class App extends Component{


    render() {
        return (
          <div className="d-flex justify-content-center div-card">
          <div className="card shadow p-3 mb-5 bg-white rounded" style={cardStyle}>
        
              <div className="card-body">
                <h5 className="card-title">Bem vindo ao portal da UNICAP</h5>
                <p className="card-text">Aqui você será capaz de acessar os dados dos professores e suas respectivas disciplinas.
                </p>
                <a>Por onde começar: </a>
                <ul>
                  <li>
                    <a className="card-link">
                      <Link to="/listar-professores">Listar professores</Link>
                    </a>
                  </li>
                </ul>
              </div>
          </div>
        </div>
        );
    }
}
export default App;

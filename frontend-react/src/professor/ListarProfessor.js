import React, {Component} from 'react';
import $ from 'jquery';
import {Professor} from '../model/Professor';
import {Button, Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap';
import MyModal from '../components/MyModal';

export class ListarProfessor extends Component {

    constructor() {
        super();
        this.state = {
            professores: [{
                nome:'',
                matricula:'',
                email:'',
                disciplinas:[
                    {
                        codigo:'',
                        descricao:'',
                        quantidadeCreditos:null
                    }
                ]
            }],
            disciplinas:[],
            requiredItem: 0

        };

    }


    componentWillMount() {
        $.ajax({
            url: 'http://localhost:3100/usuarios',
            dataType: 'json',
            success: function (res) {
                this.setState({professores: res});
            }.bind(this)
        });
    }
  
    replaceModalItem(index) {
        this.setState({
          requiredItem: index
        });
      }

    render() {
        const professoresRender = this.state.professores.map((professor, key) => {
            debugger
            return (
                <tr key={key}>
                    <td>{professor.matricula}</td>
                    <td>{professor.nome}</td>
                    <td>{professor.email}</td>
                    <td>
                        <button className="btn btn-success" data-toggle="modal" data-target="#exampleModal" 
                                onClick={()=>this.replaceModalItem(key)}>
                            Detalhes
                            
                        </button>
                    </td>
                    {this.state.teste ? <MyModal disciplinas={this.state.disciplinas}/>: <p/>}
                </tr>
            )
        });
        const item = this.state.requiredItem;
        let data = this.state.professores[item];
        return (
            <div>
                <table className="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">Matricula</th>
                        <th scope="col">Nome</th>
                        <th scope="col">Email</th>
                        <th scope="col">Disciplinas</th>
                    </tr>
                    </thead>
                    <tbody>
                        {professoresRender}
                    </tbody>
                </table>
                
                <MyModal
                    professor={data.nome}
                    disciplinas = {data.disciplinas}
                />
            </div>
        )
    }
}



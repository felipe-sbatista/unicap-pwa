import React, {Component} from 'react';
import $ from 'jquery';

export class MyModal extends Component {

    constructor(props) {
        super(props);
        this.handleSave = this.handleSave.bind(this);
        this.state = {
            title: '',
            msg: '',
            disciplinas: []
        }
    }

    
    // componentWillReceiveProps(nextProps) {
    //     this.setState({
    //         professor: nextProps.professor,
    //         disciplinas: nextProps.disciplinas
    //     });
    // }

    componentWillUpdate(props){
        if(this.props !== props)
            this.setState({
                disciplinas:props.disciplinas,
                professor:props.professor
            })
    }

    titleHandler(e) {
        this.setState({title: e.target.value});
    }

    msgHandler(e) {
        this.setState({msg: e.target.value});
    }

    discHandler(e) {
        this.setState({disciplinas: e.target.value});
    }

    handleSave() {
        const item = this.state;
        this.props.saveModalDetails(item)
    }

    render() {
        return (
            <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">Disciplinas de {this.state.professor}</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <div className="modal-body">
                        <table className="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">Codigo</th>
                                    <th scope="col">Descrição</th>
                                </tr>
                                </thead>
                                <tbody>
                                    {
                                        this.state.disciplinas.map((disc, key) => {
                                            return (
                                                <tr key={key}>
                                                    <td>{disc.codigo}</td>
                                                    <td>{disc.descricao}</td>
                                                </tr>
                                            );
                                        })
                                    }
                                </tbody>
                            </table>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default MyModal;

import React, {Component} from 'react';
import {Link} from "react-router-dom";



export class Topbar extends Component {

    render(): React.ReactElement<any, string | React.JSXElementConstructor<any>> | string | number | {} | React.ReactNodeArray | React.ReactPortal | boolean | null | undefined {
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"/>
                </button>
                <a className="navbar-brand"><Link to="/">Home</Link></a>
                <div className="collapse navbar-collapse" id="navbarTogglerDemo03">
                    <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
                        <li className="nav-item">
                            <a className="nav-link">
                                <Link to="/listar-professores">Professores</Link>
                            </a>
                        </li>
                        {/* <li className="nav-item">
                            <a className="nav-link disabled">
                                <Link to="/listar-professores">Cadastro</Link>
                            </a>
                        </li> */}
                    </ul>
                </div>
            </nav>
        );
    }
}

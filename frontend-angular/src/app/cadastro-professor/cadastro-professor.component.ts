import { Component, OnInit } from '@angular/core';
import { ProfessorService } from '../service/professor.service';
import { Professor } from '../entity/Professor';

@Component({
  selector: 'app-cadastro-professor',
  templateUrl: './cadastro-professor.component.html',
  styleUrls: ['./cadastro-professor.component.css']
})
export class CadastroProfessorComponent implements OnInit {

  model: Professor = new Professor();
  constructor(private professorService: ProfessorService) { }

  ngOnInit() {
    this.limparCampos();
  }

  enviarDados() {
    this.professorService.addProfessor(this.model).subscribe(res=>{
      alert(res);
      this.limparCampos();
    });
    
  }

  limparCampos(): void {
    this.model = new Professor();
  }

}

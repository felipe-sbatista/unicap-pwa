import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdicionarDisciplinasComponent } from './adicionar-disciplinas.component';

describe('AdicionarDisciplinasComponent', () => {
  let component: AdicionarDisciplinasComponent;
  let fixture: ComponentFixture<AdicionarDisciplinasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdicionarDisciplinasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdicionarDisciplinasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ProfessorService } from './../service/professor.service';
import { DisciplinaService } from './../service/disciplina.service';
import { Disciplina } from './../entity/Disciplina';
import { Professor } from '../entity/Professor';

import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';


@Component({
  selector: 'app-adicionar-disciplinas',
  templateUrl: './adicionar-disciplinas.component.html',
  styleUrls: ['./adicionar-disciplinas.component.css']
})
export class AdicionarDisciplinasComponent implements OnInit {

  show: boolean = true;
  mat;

  professores: Professor[];

  observableDisciplina: Observable<any>;
  disicplinas: Disciplina[];

  lista: Professor[] = [];
  professor: Professor;
  disciplinas: Disciplina[] = [];
  disciplinasToAdd: Array<Disciplina> = new Array<Disciplina>();
  constructor(private disciplinaService: DisciplinaService,
    private professorService: ProfessorService, private router: Router) { }

  ngOnInit() {
  }

  getProfessor() {
    this.professorService
      .getProfessorByMatricula(this.mat).subscribe(res => {
        this.professor = res
        if (res != null) {
          this.disciplinaService.getAllDisciplinas().subscribe(res => this.disciplinas = res);
        }
      });
  }

  addDisciplina(item: Disciplina) {
    this.disciplinasToAdd.push(item);
  }

  setDisciplinas() {
    this.professor.disciplinas = this.disciplinasToAdd;
    this.professorService.updateProfessorDisciplinas(this.professor);
    this.router.navigate(['listarProfessor']);
  }

}

import { Professor } from '../entity/Professor';
import { Component, OnInit, Input } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal-detalhes',
  templateUrl: './modal-detalhes.component.html',
  styleUrls: ['./modal-detalhes.component.css']
})
export class ModalDetalhesComponent implements OnInit {

  @Input() detalharProfessor:Professor;

  constructor(public modalService: NgbModal, public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

}

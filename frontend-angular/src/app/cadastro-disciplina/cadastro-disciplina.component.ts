import { DisciplinaService } from './../service/disciplina.service';
import { Disciplina } from './../entity/Disciplina';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cadastro-disciplina',
  templateUrl: './cadastro-disciplina.component.html',
  styleUrls: ['./cadastro-disciplina.component.css']
})
export class CadastroDisciplinaComponent implements OnInit {

  model: Disciplina = new Disciplina();
  constructor(private disciplinaService: DisciplinaService) { }

  ngOnInit() {
  }

  enviarDados(): void {
    this.disciplinaService.addDisciplina(this.model).subscribe(res => {
      alert(res);
      this.limparCampos();
    })

  }

  limparCampos(): void {
    this.model = new Disciplina();
  }

}

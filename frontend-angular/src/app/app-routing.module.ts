import { HomeComponent } from './home/home.component';
import { AdicionarDisciplinasComponent } from './adicionar-disciplinas/adicionar-disciplinas.component';
import { ListarProfessorComponent } from './listar-professor/listar-professor.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CadastroProfessorComponent } from './cadastro-professor/cadastro-professor.component';
import { CadastroDisciplinaComponent } from './cadastro-disciplina/cadastro-disciplina.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'cadastroProfessor',
    component: CadastroProfessorComponent
  },
  {
    path: 'listarProfessor',
    component: ListarProfessorComponent
  },
  {
    path: 'adicionarDisciplina',
    component: AdicionarDisciplinasComponent
  },
  {
    path: 'cadastroDisciplina',
    component: CadastroDisciplinaComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Disciplina } from './Disciplina';

export class Professor  {
    matricula: string;
    nome: string;
    email:string;
    disciplinas:Array<Disciplina>;
    id:number;

}
import { Injectable } from '@angular/core';
import { Professor } from '../entity/Professor';
import { HttpClient } from '@angular/common/http';
import { AngularFireDatabase } from '@angular/fire/database';
import { map, switchMap } from 'rxjs/operators';
import { defineBase } from '@angular/core/src/render3';
import { Subject } from 'rxjs';


const API = 'http://localhost:8080/professor';
@Injectable({
  providedIn: 'root'
})
export class ProfessorService {

  professores = new Array();
  constructor(private httpClient: HttpClient) { }

  getAllProfessores() {
    return this.httpClient.get<Professor[]>(API + '/listar');
  }

  addProfessor(professor: Professor) {
    return this.httpClient.post<Professor>(API +'/cadastrar', professor);
  }

  getProfessorByMatricula(matricula) {
    return this.httpClient.get<Professor>(API + '/buscar/' + matricula);
  }

  updateProfessorDisciplinas(professor: Professor) {
    return this.httpClient.put<Professor>(API + '/atualizar', professor);
  }
}


import { Disciplina } from './../entity/Disciplina';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const API = 'http://localhost:8080/disciplina';
@Injectable({
  providedIn: 'root'
})
export class DisciplinaService {

  disciplinas: Disciplina[];

  constructor(private httpClient: HttpClient) { }

  addDisciplina(disciplina: Disciplina) {
    return this.httpClient.post<Disciplina>(API + '/cadastrar', disciplina);
  }

  getAllDisciplinas() {
    return this.httpClient.get<Disciplina[]>(API + '/listar');
  }


}

import { ModalDetalhesComponent } from './../modal-detalhes/modal-detalhes.component';
import { Professor } from '../entity/Professor';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ProfessorService } from '../service/professor.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Disciplina } from '../entity/Disciplina';
import { Observable } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}
const ELEMENT_DATA: PeriodicElement[] = [
  { position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H' },
  { position: 2, name: 'Helium', weight: 4.0026, symbol: 'He' },
  { position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li' },
  { position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be' },
  { position: 5, name: 'Boron', weight: 10.811, symbol: 'B' },
  { position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C' },
  { position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N' },
  { position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O' },
  { position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F' },
  { position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne' },
];
@Component({
  selector: 'app-listar-professor',
  templateUrl: './listar-professor.component.html',
  styleUrls: ['./listar-professor.component.css']
})

export class ListarProfessorComponent implements OnInit {

  professores: Professor[] = [];
  constructor(private professorService: ProfessorService, private modalService: NgbModal) { }

  ngOnInit() {
    this.professorService.getAllProfessores().subscribe(res => {
      this.professores = res;
    });

  }

  openModal(professor: Professor): void {
    const modalRef = this.modalService.open(ModalDetalhesComponent);
    modalRef.componentInstance.detalharProfessor = professor;
  }

}

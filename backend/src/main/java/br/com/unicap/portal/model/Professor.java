package br.com.unicap.portal.model;

import br.com.unicap.portal.base.AbstractEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;


@Entity
@Getter
public class Professor extends AbstractEntity<Long> {


    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) Long id;

    @Setter
    private String matricula;

    @Setter
    private String nome;

    @Setter
    private String email;

    @Setter
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, targetEntity = Disciplina.class)
    @JoinTable(name="professor_disciplina",
            joinColumns=@JoinColumn(name="professor.id"),
            inverseJoinColumns=@JoinColumn(name="disciplina.id"))
    private List<Disciplina> disciplinas;
}

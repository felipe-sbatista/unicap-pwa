package br.com.unicap.portal.service;

import br.com.unicap.portal.base.AbstractService;
import br.com.unicap.portal.model.Disciplina;
import br.com.unicap.portal.repository.DisciplinaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DisciplinaService extends AbstractService<Disciplina, Long> {

    @Autowired
    private DisciplinaRepository repository;

    public Disciplina getDisciplinaByCodigo(String codigo){
        return this.repository.findByCodigo(codigo);
    }
}

package br.com.unicap.portal.repository;

import br.com.unicap.portal.model.Disciplina;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DisciplinaRepository extends JpaRepository<Disciplina, Long> {
    Disciplina findByCodigo(String codigo);
}

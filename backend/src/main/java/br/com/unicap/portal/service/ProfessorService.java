package br.com.unicap.portal.service;

import br.com.unicap.portal.base.AbstractService;
import br.com.unicap.portal.model.Professor;
import br.com.unicap.portal.repository.ProfessorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProfessorService extends AbstractService<Professor, Long> {

    @Autowired
    ProfessorRepository repository;


    public Professor getProfessorByMatricula(String matricula){
        return this.repository.findByMatricula(matricula);
    }
}

package br.com.unicap.portal.repository;

import br.com.unicap.portal.model.Professor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfessorRepository extends JpaRepository<Professor, Long> {

    Professor findByMatricula(String matricula);
}

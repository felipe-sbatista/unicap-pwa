package br.com.unicap.portal.model;

import br.com.unicap.portal.base.AbstractEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
public class Disciplina extends AbstractEntity<Long> {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY) Long id;


    @Setter
    private String codigo;

    @Setter
    private String descricao;

    @Setter
    private int quantidadeCreditos;

    @Setter
    @ManyToMany(targetEntity = Professor.class, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name="professor_disciplina",
            joinColumns=@JoinColumn(name="disciplina.id"),
            inverseJoinColumns=@JoinColumn(name="professor.id"))
    private List<Professor> professores;
}

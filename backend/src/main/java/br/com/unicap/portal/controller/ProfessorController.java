package br.com.unicap.portal.controller;

import br.com.unicap.portal.model.Professor;
import br.com.unicap.portal.service.ProfessorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/professor")
@CrossOrigin("http://localhost:4200")
public class ProfessorController {

    @Autowired
    private ProfessorService service;

    @PostMapping("/cadastrar")
    public ResponseEntity<?> cadastrar(@RequestBody Professor professor) {
        return ResponseEntity.ok(this.service.salvar(professor));
    }

    @GetMapping("/listar")
    public ResponseEntity<?> listarTodos() {
        return ResponseEntity.ok(this.service.listarTodos());
    }

    @PutMapping("/atualizar")
    public ResponseEntity<?> atualizar(@RequestBody Professor professor) {
        return ResponseEntity.ok(this.service.atualizar(professor));
    }

    @GetMapping("/buscar/{matricula}")
    public ResponseEntity<?> buscarByMatricula(@PathVariable String matricula) {
        Professor professor = this.service.getProfessorByMatricula(matricula);
        if (professor == null) {
            return ResponseEntity.ok("Professor não cadastrado");
        }
        return ResponseEntity.ok(professor);
    }

}
package br.com.unicap.portal.controller;

import br.com.unicap.portal.model.Disciplina;
import br.com.unicap.portal.service.DisciplinaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/disciplina")
@CrossOrigin("http://localhost:4200")
public class DisciplinaController {

    @Autowired
    private DisciplinaService service;

    @PostMapping("/cadastrar")
    public ResponseEntity<?> cadastrar(@RequestBody Disciplina disciplina){
        return ResponseEntity.ok(this.service.salvar(disciplina));
    }

    @GetMapping("/listar")
    public ResponseEntity<?> listarTodos(){
        return ResponseEntity.ok(this.service.listarTodos());
    }

    @PutMapping("/atualizar")
    public ResponseEntity<?> atualizar(@RequestBody Disciplina disciplina){
        return ResponseEntity.ok(this.service.atualizar(disciplina));
    }

    @GetMapping("/buscar/{codigo}")
    public ResponseEntity<?> buscarByCodigo(@PathVariable String codigo){
        Disciplina disciplina = this.service.getDisciplinaByCodigo(codigo);
        if(disciplina == null){
            return ResponseEntity.ok("disciplina não cadastrada");
        }
        return ResponseEntity.ok(disciplina);
    }

}